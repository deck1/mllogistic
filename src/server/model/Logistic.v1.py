# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from numpy import arange
from sklearn import tree
from sklearn import preprocessing
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import cross_validate
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from fpdf import FPDF 
from sklearn.ensemble import StackingClassifier
from sklearn.pipeline import Pipeline
from sklearn.experimental import enable_hist_gradient_boosting
from keras.models import *
import pickle
import datetime
from datetime import datetime as dt
from datetime import timedelta
import io
import boto3
# import pickle5 as pickle

# %%
pd.options.display.max_columns = None
pd.options.display.max_rows = None

# %%
s3 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com")

# %%
print("----load start------")
data=s3.Object('upload-zszljfwd','loan_f1.pickle').get()['Body'].read()
df_raw=pd.read_pickle(io.BytesIO(data),compression=None)
print("---load done----")

# %%
print("----copy start------")
df=df_raw.copy()
print("----copy finish------")

# %%
print("----Shape------")
print(df.shape)

# %%
print("----Split------")
X = df.drop('ZZ_STATUS', axis=1)
y = df['ZZ_STATUS'].values
print("----Split Done ------")

# %%
print("----Split Again------")
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.20, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train,y_train,test_size=0.25, random_state=0)
print("----Split Done ------")

# %%
print("----Doing SMOTE------")
sm = SMOTE()
X_train_smote, y_train_smote = sm.fit_resample(X_train, y_train)
print("----Done SMOTE------")

# %%
def build_logistic_regression():
    print("Starting ------ Logistic Regression")
    start_ts=datetime.datetime.now() 
    pipe = Pipeline(steps=[
        ('logistic', LogisticRegression())
    ])
    
    param_grid ={
        'logistic__penalty':('l1', 'l2', 'elasticnet', 'none'),
        'logistic__solver':('newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'),
        'logistic__max_iter':[50,100,500,1000]
    }
    model=GridSearchCV(estimator=pipe,
                             param_grid=param_grid,
                             scoring='roc_auc', 
                             pre_dispatch='2*n_jobs', 
                             cv=5, 
                             verbose=1,
                             return_train_score=False)
    
    model.fit(X_train_smote,y_train_smote)
    pkl_filename = "./model/logistic_regression.pkl"
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)

    X_val_np = X_val.to_numpy()
    predicted = model.predict(X_val_np)
    proba = model.predict_proba(X_val_np)
    
    precision=precision_score(y_val, predicted, average='weighted')
    recall=recall_score(y_val, predicted, average='weighted')
    f1=f1_score(y_val, predicted, average='weighted')
    accuracy=accuracy_score(y_val, predicted)
    
    CM = confusion_matrix(y_val, predicted)
    (TN,FN,TP,FP) = (CM[0][0],CM[1][0],CM[1][1],CM[0][1])
    FPR = FP/(FP+TN)
    
    end_ts=datetime.datetime.now()
    delta=(end_ts-start_ts)
    
    COLUMNS=['Algorithm','Accuracy','Precision','Recall','F1-support','FPR','RUNTIME','PROBA','PREDICT','ESTIMATOR','BEST PARAMS']
    dic=[['Logistic Regression',accuracy,precision,recall,f1,FPR,str(delta),proba,predicted,model.best_estimator_,model.best_params_]]
    df=pd.DataFrame(dic,columns=COLUMNS)
    
    print("Best Params-")
    print(model.best_params_)
    
    
    return df

# %%
df=build_logistic_regression()
print(df)